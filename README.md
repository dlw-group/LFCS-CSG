SFS Linux Camp and LFCS Study Group
---

This study group will help the student to sharpen up before Linux Camp and/or before facing the Linux Foundation Certified Sysadmin exam.

To flourish in the real world, you must be competent, resourceful, and relational.

To pass the exam, you must be competent, resourceful, and very fast.

Practice, comprehend, practice, explore, practice, and share.


LFCS rocks and sucks
---

**What rocks?**
* distribution-agnostic
* practical exam, not multiple choice
* less expensive than RHCSA
* mostly germane bits

**What sucks?**
 * more expensive than LPIC-1
 * external sources (notes, books, Internet) not allowed
 * some esoteric bits


The fast and mighty fist of undertanding
---

 - L. some-binary --help
 - R. man/info something
 - M. /usr/share/doc/somepackage
 - P. www.some-project.org
 - T. DuckDuck/Google something


Values for Story-based Scenarios
---

```
Company: RackWizards
CEO: Jan
CTO: Kim
Staff: Alice, Bob, Charlie
Consultants: Dani, Eve, Fred
Admins: Kim, Alice, Bob, Eve
Accounting: Jan, Dani, Eve
```


To Do List
---
 - nfs
 - samba?
 - cron and archives: schedule backups, software from source


Completed Sessions
---
**Session 7 - 2016 Aug 04** - [Partial Recording](
  http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1470279572883) - [ Notes ]( Session_7_Notes.md )
- LAMP - Apache WebServer
- virtual hosts
- basic authentication

**Session 6 - 2016 Jul 27** - [Mute Recording](
  http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1469667306296
  ) - [ Notes ]( Session_6_Notes.md )
- cron
- tar
- gzip

**Session 5 - 2016 Jul 20** - [ Recording ]( http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1469062550885 ) - [ Notes ]( Session_5_Notes.md )
- LAMP - Apache WebServer
- virtual hosts
- basic authentication

**Session 4 - 2016 Jul 06** - [ Recording ]( http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1467852791692 ) - [ Notes ]( Session_4_Notes.md )
- local storage: disks, partitions, file-systems
- LVM: physical volumes, volume groups, logical volumes
- /etc/fstab

**Session 3 - 2016 Jun 29** - [ Recording ]( http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1467244140231 ) - [ Notes ]( Session_3_Notes.md )
- sudoers
- account expiry
- some discussion of packages, repositories, and stacks

**Session 2 - 2016 Jun 22** - [ Recording ]( http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1466642905770 ) - [ Notes ]( Session_2_Notes.md ) - [ Files ]( Session2-files )
- users
- groups

**Session 1 - 2016 Jun 15** - [ Recording ]( http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1466038248494 ) - [ Notes ]( Session_1_Notes.md )
- hostnames and static IP's

**Session 0 - 2016 Jun 8** - [ Recording ]( http://107.189.32.195/playback/presentation/0.9.0/playback.html?meetingId=2756eb09bffdb2e4cf8ebbb795e4d8326516dfa8-1465433097148 ) - [ Notes ]( Session_0_Notes.md )
- overview LFCS
- build machines
- introductions
