Agenda
---

What is Free Software and why is freedom important to art and science?
What is SFS?
What is Linux?
What is the Linux Foundation?
What is a Linux Foundation Certified SysAdmin certified to be able to *do*, exactly?
What is the exam like?
On which Linux distributions can you choose to take the exam?
How will we study? (Virtualbox or other virtualization software)


About the LFCS
---

Compare LFCS to RHCSA and LPIC-1 at Google Trends: https://www.google.com/trends/explore#q=LFCS%2C%20LPIC-1%2C%20RHCSA&cmpt=q&tz=Etc%2FGMT%2B6


Studying
---

Pick a Linux distribution and version from those available for the exam. Go [here]( https://training.linuxfoundation.org/certification/lfcs ), click "REGISTER FOR THE EXAM", and on the next page, open "Distro Choice for your exam".

Choose your favorite from those listed and build one or more servers of that distro and version.

Build a full graphical workstation of the latest version of the parent of your exam distribution. (For CentOS, latest Fedora. For Ubuntu, latest debian. For openSUSE, latest openSUSE)

Optionally, build additional servers and workstations of other distros that interest you.


Content of the LFCS
---

### Editing

Be proficient with your editor of choice. Mine is vim. I will talk through my use of keys.

Exercises: Write scripts and (markdown) docs.

### Finding files and content

Be proficient with find and grep.

 * 'find' files by size/name/ownership/perms
 * 'grep' recursively to find files by content

### Compression and Archiving

Be competent with tar, zip, bzip2, gzip, etc.

Exercises: Backups, restores, installs from source (unpack, configure, make, make install)

### Manage Packages and Package Repositories

rpm/yum, dpkg/apt, rpm/zypper, source installs

Exercises:

What packages do I have?

Which package owns (file)?

Which available package would provide "*bin/httpd"?

Also consider groups (yum), tasks (apt/tasksel), bundles? (zypper)

### Manage Local Users and Groups

 * Create and remove users and groups: change /etc/skel, change shell and/or home-dir
 * Add users to secondary groups.
 * Expire/unexpire/suspend users.
 * Set memory and CPU limits. (nprocs)
 * visudo, /etc/sudoers
 * /etc/skel


### Manage File Permissions

chmod


### Manage Local Storage and File-Systems

 * fdisk/cfdisk/parted
 * lvm - pv*,vg*,lv*
 * mkfs*
 * /etc/fstab
 * mount/umount


### Manage Network File-Systems (client ops)

 * nfs
 * samba (optional)


### Schedule jobs

 * cron
 * at (optional)


### Script

basic bash scripts


### Networking

 * configure static network settings
 * manipulate hosts entries


Notes from while I was studying for the exam
---

[ LFCS Domains and Competencies V2.16 ]( https://training.linuxfoundation.org/upcoming-program-changes-for-the-lfcs-certification-exam )

**Distributions:**

* CentOS 7
* OpenSUSE 13.1
* Ubuntu 14.04

Someone else's study repo: https://github.com/xezpeleta/lfcs

TecMint's resouces. They're "for free" anyway. I don't know if they're "free as in freedom".

 * LFCS starts here: http://www.tecmint.com/sed-command-to-create-edit-and-manipulate-files-in-linux/
 * LFCE starts here: http://www.tecmint.com/installing-network-services-and-configuring-services-at-system-boot/

Official Mats? [ LFS201 LABS ]( https://lms.360training.com/scorm/linuxfoundation/LFS201/LFS201%20-%20LABS.pdf )

[ SystemD FAQ ]( https://www.freedesktop.org/wiki/Software/systemd/FrequentlyAskedQuestions/ )

 * Pay attention to targets, which are like SysV init runlevel"
 * For dlwillson@suse-w1, sometimes "isolate" doesn't work, but "start" does:
   no:
   - `sudo systemctl isolate hibernate.target`
   yes:
   - `sudo systemctl start hibernate.target`
   - `sudo systemctl isolate reboot.target`
   - `sudo systemctl start reboot.target`
   - `sudo systemctl isolate multi-user.target`
   - `sudo systemctl isolate graphical.target`

** Install, configure, and troubleshoot the bootloader. **

I zeroed the first 100 bytes of the disk, then used a mod of [ this process ]( https://forums.opensuse.org/content.php/128-Re-install-Grub2-from-DVD-Rescue?page=4 ) to recover:

1. boot from DVD to rescue
2. `mount /dev/sda2 /mnt`
3. `mount -o bind /proc /mnt/proc`
4. `mount -o bind /sys /mnt/sys`
5. `mount -o bind /dev /mnt/dev`
6. `chroot /mnt`
7. `grub2-install /dev/sda`

** Manage shared libraries **

http://www.ibm.com/developerworks/library/l-lpic1-102-3/

note ldd ldconfig /etc/ld.so.conf*
