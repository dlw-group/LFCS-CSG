*Before* this session, you should have:

1. Installed or updated VirtualBox or your desktop virtualization platform of choice.
2. Chosen your exam distribution: CentOS 7, openSUSE 13.1, or Ubuntu Server 14.04.
3. Built a server VM: minimal, command-line only server with the distribution you chose in #2.
   Optionally, built more server VM's.
4. Built a workstation VM: This should be a full GUI machine.
   I recommend using your exam distribution or the upstream "cutting edge" of your exam distribution.
   For CentOS 7, the cutting edge is the latest Fedora.
   For openSUSE 13.1, it's the latest openSUSE. Tumbleweed is slightly edgier than Leap.
   For Ubuntu Server 14.04, it's Ubuntu 16.04 or the latest Debian. I think Debian is edgier.
   Optionally, built more workstation VM's.
   Optionally, confirmed that all your VM's can ping one another.
5. Reviewed the study group repository content here: https://gitlab.com/sofreeus/LFCS-CSG/tree/master
   Optionally, git-clone'd the repository to your workstation VM and/or your workstation.

Gifts: Did everyone that is not a Linux Camper put in a gift?

Buddy Program: Everyone pick two buddies. Make sure your buddies are keeping up with the study group. Need email/phone correspondents for CO.


Networking Stuph
---

Configure your servers with static hostnames and IPv4 addresses and verify that you can ssh to them from your workstation by name.

For each server:
 - set hostname
 - configure static ip address
 - confirm ssh server is installed, enabled, and started
 - add new name to /etc/hosts as 127.0.1.1
 - add hosts entry to your workstation

Useful binaries and config files:
 - ip (ip a, ip neigh, ip route, etc.)
 - nmtui
 - yast
 - files
    CentOS:   /etc/sysconfig/network-scripts/ifcfg-(interface-name)
    openSUSE: /etc/sysconfig/network/ifcfg-(interface-name)
    Ubuntu:   /etc/network/interfaces
 - /etc/hosts
 - systemctl
 - journalctl

Discuss bonding, teaming, DHCP, DNS, routing, NAT, iptables/firewalld


Users and Groups Stuph (next week)
---

- add users
- add groups
- set expiration on contractors
- set admins as global sudoers (NOPASSWD)

- write a script to do all that

Discuss configuration drift and solutions:

 - chef/puppet/ansible/salt
 - AD & Centrify
 - FreeIPA / IDM
 - generic LDAP & kerberos


Package Management
---

What provides X (binary or other file)?
What package does (file) belong to?

