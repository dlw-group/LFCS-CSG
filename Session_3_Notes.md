Session 3
---

Did you *really* get a study buddy or two?
 - Yes, I did! :-)
 - No, not really... :-(

How often are you and your budd(y|ies) checking in with each other?
 - 2 - Twice or more each week
 - 1 - About once per week
 - 0 - Sometimes we see each other in BBB

sudoers and file permissions

1. set expiration on contractors
2. set admins as global sudoers (NOPASSWD)
3. Create a public RW folder: /home/shared/
    - everyone can create files and folders
    - but only the owning user can delete their files/folders
4. Create a group folder: /home/shared/admins/
    - group-members can RW each other's documents
    - ^ folder is setgid
    - others can read files
5. Create a private group folder: /home/shared/admins/private/
    - group-members can create files, but cannot write to each other's files
    - ^ folder is NOT setgid
    - others have no access


binaries used:
* chmod
  - use symbolic notation
  - don't use numeric unless intentionally resetting all perms
* chown
* mkdir [-p]
* ssh, ssh-keygen, ssh-copyid
  - set a decent password on your personal private key
* sftp/scp/rsync
* chage
* umask
* touch
  - and used a null redirector '> somefile' as a fakey touch


files changed:
* /etc/shadow
* /etc/sudoers
