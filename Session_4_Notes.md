Review:
---

Review users, groups, sudo.

Does anyone want to show off their script/repo?

Discuss database sync and configuration drift.

Solutions (CM):
 - chef
 - puppet
 - ansible
 - salt

Solutions (directory/auth):
 - AD & Centrify
 - FreeIPA / IDM
 - generic LDAP & kerberos


Talk:
---

[ Linux FHS ]( http://www.linuxfoundation.org/collaborate/workgroups/lsb/fhs-30 )
/usr/, /sbin/, /bin/, /home/, /var/, /srv/, /etc/

Storage:

* a disk gets partitions
* a partition gets a filesystem, or added to mdraid, or added to LVM
* LVM:
  - a partition gets pvcreate'd
  - pv's get aggregated into a vg
  - a vg gets sliced into lv's
  - an lv gets a filesystem
* filesystems get mounted onto a mount-point directory
* filesystem attachments that will be made many times should be registered in /etc/fstab

finding files:
- locate is fast but only finds files by name, in an index (slocate.db)
- find is slower but finds files by any combo of meta-data: name, owner, location, group, permission, size, changed and modified timestamps
- grep ( with --recursive ) can be used to find files by content

Demo, Do, and Share:
---

Create a dedicated file-system for backups.

1. Add a disk to a server
2. Add a partition of type 8e
3. create a pv of the partition
4. if needed or desired, create a vg
5. add the pv to the vg
6. create a new lv named 'lv_backups'
7. add a resizeable filesystem to the lv
8. create an immutable mount-point directory: /backups
9. mount the fs and umount it
10. register the mount in /etc/fstab and test

Programs:

* fdisk, cfdisk, parted, gparted
* mount, umount
* lsblk, blkid
* pvcreate, vgcreate, lvcreate, lvextend
* mkfs.*
* resize2fs

Config Files:

* /etc/fstab


Still to do:
---

 * cron, tar, gzip, etc...
 * NFS, samba?
 * Apache, LAMP
    - zypper install pattern lamp-server
    - yum groupinstall ???
    - tasksel? (how to use tasks from apt / apt-get)
