# LAMP Stack

* _L_inux
* _A_pache
* _M_ariaDB (or _M_ySQL)
* _P_HP

We are doing _L_inux and we will cover _A_pache and _P_HP.

# Apache

CentOS 7 / RHEL 7:

    sudo yum group install "Web Server"

Start / Enable service:

    sudo systemctl enable httpd
    sudo systemctl start httpd

Poke holes in firewall

    sudo firewall-cmd --add-service=http --permanent
    sudo firewall-cmd --reload

Point browser to http://<ipaddr>/

Huzzah!

# Virtual Hosts

It's like NAT? for websites

    www.example.com --\                /-- /var/www/www.example.com
    blog.example.com --- 198.51.100.5 ---- /var/www/blog.example.com
    www.sofree.us ----/                \-- /var/www/www.sofree.us

Example config: /usr/share/doc/httpd-<version>/httpd-vhosts.conf

Create two vhosts:

* private.rackwizards.example
* www.rackwizards.example (alias: rackwizards.example)

# Basic auth

Secure private.rackwizards.example, only Jan and Kim have access.

    <Location "/secure">
        AuthType basic
        AuthName "private area"
        AuthUserFile /etc/httpd/private-passwd
        Require valid-user
    </Location>

Now, create passwd file

    htpasswd -c /etc/httpd/private-passwd jan
    htpasswd /etc/httpd/private-passwd kim

Did we make a mistake?

    apachectl configtest

OK, let's reload the config

    systemctl reload httpd

Verify you can login to private.rackwizards.example

# PHP!

PHP is super easy to install:

    yum install php
    systemctl restart httpd

Create file info.php with contents

    <?php phpinfo(); ?>

Done!

Would you like to know more?
about [vhosts](https://httpd.apache.org/docs/2.4/vhosts/examples.html)
