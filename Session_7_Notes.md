NFS (and maybe Samba)
===

### plan
* Install server and set up export
  - discuss host-based security
  - research tcp wrappers
* Mount export locally
* Mount from client
* Mount from another client
* set up samba share maybe
  - mount locally with mount.cifs

```shell
sudo yum groups list --verbose
sudo yum groups install file-server # installs support for NFS, CIFS, and iSCSI

sudo systemctl list-unit-files | grep nfs
sudo systemctl enable nfs-server
sudo systemctl start nfs-server
sudo mkdir /srv/shared
sudo chmod a+rwx /srv/shared/
sudo exportfs *:/srv/shared/
showmount -e localhost
sudo mount localhost:/srv/shared /mnt
ls /mnt
touch /mnt/test
ls -lF /mnt
sudo umount /mnt
man exports
man exports | grep insecure,all_squash | sudo tee -a /etc/exports
sudo vim /etc/exports
# /srv/shared *(rw,all_squash,anon_uid=99,anon_gid=99)
# ^ try to match anons of Samba, if setting up dual-shared public
# root_squash, the default, changes the root user to the anon user over nfs
# all_squash changes *all* users to the anon user over nfs
sudo systemctl restart nfs-server
# research autofs!

# NFS client actions
# install nfs-utils?
sudo mkdir /shared
# make mount points immutable right after you create (David Willson preference)
sudo chattr +i /shared
sudo mount (server-name-or-ip):/srv/shared /shared
grep shared /proc/mounts
grep shared /proc/mounts | sudo tee -a /etc/fstab
sudo vim /etc/fstab
# server:/srv/shared /shared nfs4 defaults 0 0
sudo umount /shared
sudo mount /shared
```

samba
---

```shell
rpm -qa (query and list all packages)
rpm -qa | grep samba
rpm -ql samba # list files in samba package

cat /etc/*-release (describe which distro you are on, versions, code names)

vim /etc/samba/smb.conf

sudo systemctl list-unit-files | grep smb
sudo systemctl start smb
man 8 samba_selinux # https://fedoraproject.org/wiki/SELinux/samba
# - especially, the bit about public_content_t for sharing files across selinux domains
# anonymous smbclient works, but anonymous mounts don't... Is this fixable?
# autofs really rocks with NFS. I should teach it over a lunch-and-learn.
```
